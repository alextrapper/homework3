import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class Main {
    private static WebDriver driver;
    private static TestData data = new TestData();
    private static TestElements locators = new TestElements();
    private static WebDriverWait wait;



    public static void main(String[] args){

        SetupWebDriver();
        LoginToAdminArea();
        InsertPlayer(data.player1);
        AssertPlayerFields(data.player1);
        ChangePlayersFields(data.player1, data.player2);
        AssertPlayerFields(data.player2);
        Logout();

    }

    private static void SetupWebDriver() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver,10);
    }

    private static void LoginToAdminArea(){
        driver.get(data.serverAddress);
        driver.findElement(By.id(locators.fieldLoginNameId)).sendKeys(data.adminLogin);
        driver.findElement(By.id(locators.fieldLoginPasswordId)).sendKeys(data.adminPassword);
        driver.findElement(By.id(locators.btnLoginId)).click();
    }

    private static void InsertPlayer(PockerPlayer player){
        Assert.assertEquals(driver.getTitle(), locators.titleAdminArea);
        driver.findElement(By.xpath(locators.btnInsertPlayerXPath)).click();

        wait.until(ExpectedConditions.elementToBeClickable(
                By.id(locators.fieldInsertUsernameId)));
        Assert.assertEquals(driver.getTitle(), locators.titleInsertPlayerPage);

        driver.findElement(By.id(locators.fieldInsertUsernameId)).sendKeys(player.getUserName());
        driver.findElement(By.id(locators.fieldInsertEmailId)).sendKeys(player.getEmail());
        driver.findElement(By.id(locators.fieldInsertPasswordId)).sendKeys(player.getPassword());
        driver.findElement(By.id(locators.fieldInsertConfirmPasswordId)).sendKeys(player.getPassword());
        driver.findElement(By.id(locators.fieldInsertFirstName)).sendKeys(player.getFirstName());
        driver.findElement(By.id(locators.fieldInsertLastName)).sendKeys(player.getLastName());
        driver.findElement(By.id(locators.fieldInsertCity)).sendKeys(player.getCity());
        driver.findElement(By.id(locators.fieldInsertAddress)).sendKeys(player.getAddress());
        driver.findElement(By.id(locators.fieldInsertPhone)).sendKeys(player.getPhone());
        driver.findElement(By.id(locators.btnSubmitNewUserId)).click();
    }


    private static void AssertPlayerFields(PockerPlayer player){
        String userEmail, userFirstName, userLastName, userCity, userAddress, userPhone;

        SearchPlayerAndOpentoEdit(player);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldEditEmailId)));
        userEmail = driver.findElement(By.id(locators.fieldEditEmailId)).getAttribute("value");
        userFirstName = driver.findElement(By.id(locators.fieldEditFirsNameId)).getAttribute("value");
        userLastName = driver.findElement(By.id(locators.fieldEditLastNameId)).getAttribute("value");
        userCity = driver.findElement(By.id(locators.fieldEditCityId)).getAttribute("value");
        userAddress = driver.findElement(By.id(locators.fieldEditAddressId)).getAttribute("value");
        userPhone = driver.findElement(By.id(locators.fieldEditPhoneId)).getAttribute("value");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(userEmail,player.getEmail());
        softAssert.assertEquals(userFirstName,player.getFirstName());
        softAssert.assertEquals(userLastName,player.getLastName());
        softAssert.assertEquals(userCity,player.getCity());
        softAssert.assertEquals(userAddress,player.getAddress());
        softAssert.assertEquals(userPhone,player.getPhone());
        softAssert.assertAll();

        //driver.findElement(By.id(locators.btnEditSubmitId)).click();
        driver.findElement(By.id(locators.btnEditCancelId)).click();
        driver.switchTo().alert().accept();
    }

    private static void ChangePlayersFields(PockerPlayer player1, PockerPlayer player2){
        SearchPlayerAndOpentoEdit (player1);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldEditEmailId)));
        WebElement email = driver.findElement(By.id(locators.fieldEditEmailId));
        WebElement f_name = driver.findElement(By.id(locators.fieldEditFirsNameId));
        WebElement l_name = driver.findElement(By.id(locators.fieldEditLastNameId));
        WebElement city = driver.findElement(By.id(locators.fieldEditCityId));
        WebElement address = driver.findElement(By.id(locators.fieldEditAddressId));
        WebElement phone = driver.findElement(By.id(locators.fieldEditPhoneId));

        email.clear();
        email.sendKeys(player2.getEmail());
        f_name.clear();
        f_name.sendKeys(player2.getFirstName());
        l_name.clear();
        l_name.sendKeys(player2.getLastName());
        city.clear();
        city.sendKeys(player2.getCity());
        address.clear();
        address.sendKeys(player2.getAddress());
        phone.clear();
        phone.sendKeys(player2.getPhone());
        driver.findElement(By.id(locators.btnEditSubmitId)).click();
        wait.until(ExpectedConditions.titleIs(locators.titleAdminArea));
    }

    private static void SearchPlayerAndOpentoEdit (PockerPlayer player){
        String btnAdminEditXPath = locators.getbtnAdminEditXPath(player);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldAdminPlayerNameId)));

        Assert.assertEquals(driver.getTitle(), locators.titleAdminArea);
        driver.findElement(By.id(locators.fieldAdminPlayerNameId)).sendKeys(player.getUserName());
        driver.findElement(By.xpath(locators.btnAdminSearchXPath)).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(btnAdminEditXPath)));
        driver.findElement(By.xpath(btnAdminEditXPath)).click();
        wait.until(ExpectedConditions.titleIs(locators.titleEditPlayerPage));
    }

    private static void Logout(){
        driver.findElement(By.xpath(locators.btnLogoutXPath)).click();
    }

}
