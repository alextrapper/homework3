class TestElements {

    // Fields
    final String fieldLoginNameId = "username";
    final String fieldLoginPasswordId = "password";

    final String fieldAdminPlayerNameId = "login";

    final String fieldInsertUsernameId = "us_login";
    final String fieldInsertEmailId = "us_email";
    final String fieldInsertConfirmPasswordId = "confirm_password";
    final String fieldInsertPasswordId = "us_password";
    final String fieldInsertFirstName = "us_fname";
    final String fieldInsertLastName = "us_lname";
    final String fieldInsertCity = "us_city";
    final String fieldInsertAddress = "us_address";
    final String fieldInsertPhone = "us_phone";

    final String fieldEditEmailId = "us_email";
    final String fieldEditFirsNameId = "us_fname";
    final String fieldEditLastNameId = "us_lname";
    final String fieldEditCityId = "us_city";
    final String fieldEditAddressId = "us_address";
    final String fieldEditPhoneId = "us_phone";


    final String fieldMessages = "messages";



    //Buttons
    final String btnLoginId = "logIn";

    final String btnAdminSearchXPath = "//button[@type=\"submit\"]";
    final String btnInsertPlayerXPath = "//a[@href=\"/players/insert\"]";
    final String btnSubmitNewUserId = "Submit";
    String getbtnAdminEditXPath (PockerPlayer playerName){
        return "//a[text()='" + playerName.getUserName() + "']//..//..//a[@title='Edit']";
    }
    final String btnEditCancelId = "Cancel";
    final String btnEditSubmitId = "Submit";
    final String btnLogoutXPath = "//a[contains(@href, 'logout')]";



    // Titles
    final String titleAdminArea = "Players";
    final String titleInsertPlayerPage = "Players - Insert";
    final String titleEditPlayerPage = "Players - Edit";





}
