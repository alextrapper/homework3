
import java.util.Objects;


class PockerPlayer {
    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String phone;


    PockerPlayer(String userName, String password, String email, String firstName,
                        String lastName, String city, String address, String phone) {
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.address = address;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "PockerPlayer{" +
                "login='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PockerPlayer that = (PockerPlayer) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(email, that.email) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(city, that.city) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, email, firstName, lastName, city, address, phone);
    }

    String getUserName() {
        return userName;
    }

    String getPassword() {
        return password;
    }

    String getEmail() {
        return email;
    }

    String getFirstName() {
        return firstName;
    }

    String getLastName() {
        return lastName;
    }

    String getCity() {
        return city;
    }

    String getAddress() {
        return address;
    }

    String getPhone() {
        return phone;
    }
}
